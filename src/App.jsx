import { useState } from 'react';
import debounce from 'lodash.debounce';
import { useCallback } from 'react';

export function App() {
  const [searchText, setSearchText] = useState('');

  const sendSearchString = useCallback(
    debounce((searchString) => {
      // logic code
      console.log(searchString);
    }, 500),
    []
  );

  const handleChange = (e) => {
    setSearchText(e.target.value);
    sendSearchString(e.target.value);
  };
  return (
    <div className="App">
      <form>
        <input
          className="searchInput"
          type="text"
          placeholder="search"
          name="search"
          onChange={handleChange}
          value={searchText}
        />
      </form>
    </div>
  );
}
